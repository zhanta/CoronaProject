'use strict';

const express = require('express')
const app = express()
const path = require('path')
const { nextTick } = require('process')
const PORT = 3000
const HOST = '0.0.0.0'

//MAIN PAGE
app.get('/', (req,res) => {
    var options = {
        root: path.join(__dirname)
    }
    var fileName = 'index.html'

    res.sendFile(fileName, options, (err) => {
        if(err) {
            next(err)
        } else {
            console.log('Sent:', fileName)
        }
    })

})

//PORTS
app.listen(PORT, HOST, (err) => {
    if(err) {
        console.log(err)
    } else {
        console.log(`App is listening at http://${HOST}:${PORT}`)
    }
})